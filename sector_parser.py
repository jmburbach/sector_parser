#!/usr/bin/env python3
# Copyright (C) 2012 Jacob Burbach <jmburbach@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# http://www1.metacraft.com/VRC/docs/doc.php?page=appendix_g
# http://www1.metacraft.com/VRC/docs/doc.php?page=appendix_f

import re, weakref


class ParseError(Exception):
	pass


class SectorColor:
	REGEX = re.compile(r"#define\s*(?P<name>\S*)\s*(?P<value>\d*)")

	def __init__(self, name, red, blue, green):
		self.name = name
		self.red = red
		self.blue = blue
		self.green = green
		self.packed_value = self.blue << 16 | self.green << 8 | self.red

	@staticmethod
	def parse(parser):
		line = parser.current_line.strip()
		match = SectorColor.REGEX.match(line)
		if not match:
			parser.raise_parse_error("invalid color definition")
		name = match.group("name")
		value = int(match.group("value"))
		r = value & 0xFF
		g = (value >> 8) & 0xFF
		b = (value >> 16) & 0xFF
		return SectorColor(name, r, g, b)


class SectorInfo:

	def __init__(self, filename, callsign, airport, lat, lon, nm_per_lat, nm_per_lon, magvar, scale):
		self.filename = filename
		self.callsign = callsign
		self.airport = airport
		self.lat = lat
		self.lon = lon
		self.nm_per_lat = nm_per_lat
		self.nm_per_lon = nm_per_lon
		self.magvar = magvar
		self.scale = scale

	@staticmethod
	def parse(parser):
		attrs = {}
		attr_names = [
			"filename",
			"callsign",
			"airport",
			"lat",
			"lon",
			"nm_per_lat",
			"nm_per_lon",
			"magvar",
			"scale"
		]
		for name in attr_names:
			if parser.eof() or parser.next_line_starts_section():
				parser.raise_parse_error("INFO section ends prematurely")
			line = parser.get_next_line().strip()
			attrs[name] = line

		filename = attrs["filename"]
		callsign = attrs["callsign"]
		airport = attrs["airport"]
		lat, lon = parser.parse_string_coords(attrs["lat"], attrs["lon"])
		nm_per_lat = float(attrs["nm_per_lat"])
		nm_per_lon = float(attrs["nm_per_lon"])
		magvar = float(attrs["magvar"])
		scale = float(attrs["scale"])
		
		return SectorInfo(filename, callsign, airport, lat, lon, nm_per_lat, nm_per_lon, magvar, scale)


class SectorNavaid:
	# name lat lon => FIX
	REGEX_3 = re.compile(r"^(\S*)\s*(\S*)\s*(\S*)$")
	# name freq lat lon => NDB VOR
	REGEX_4 = re.compile(r"^(\S*)\s*(\S*)\s*(\S*)\s*(\S*)$")

	def __init__(self, **kwds):
		self.name = kwds.get("name", "")
		self.frequency = kwds.get("frequency", "")		
		self.lat = kwds.get("lat", "")
		self.lon = kwds.get("lon", "")

	def parse_line(self, parser):
		line = parser.current_line.strip()
		m3 = SectorNavaid.REGEX_3.match(line)
		m4 = SectorNavaid.REGEX_4.match(line)
		if not m3 and not m4:
			parser.raise_parse_error("bad navaid entry")

		if m3: # fix
			self.name, self.lat, self.lon = m3.groups()
		else: # ndb, vor
			self.name, self.frequency, self.lat, self.lon = m4.groups()


class SectorAirport:
	REGEX = re.compile(r"(\S*)\s*(\S*)\s*(\S*)\s*(\S*)\s*(\S*)")

	def __init__(self, **kwds):
		self.name = kwds.get("name", "")
		self.frequency = kwds.get("frequency", "")
		self.lat = kwds.get("lat")
		self.lon = kwds.get("lon")
		self.airspace = kwds.get("airspace", "")

	def parse_line(self, parser):
		line = parser.current_line.strip()
		match = SectorAirport.REGEX.match(line)
		if not match:
			parser.raise_parse_error("bad airport entry")
		self.name, self.frequency, self.lat, self.lon, self.airspace = match.groups()


class SectorRunway:
	REGEX = re.compile(r"^(\S*)\s*(\S*)\s*(\S*)\s*(\S*)\s*(\S*)\s*(\S*)\s*(\S*)\s*(\S*)$")

	def __init__(self, **kwds):
		self.name = kwds.get("name", "")
		self.opposite_name = kwds.get("opposite_name", "")
		self.heading = kwds.get("heading", 0)
		self.opposite_heading = kwds.get("opposite_heading", 0)
		self.lat1 = kwds.get("lat1", 0)
		self.lon1 = kwds.get("lon1", 0)
		self.lat2 = kwds.get("lat2", 0)
		self.lon2 = kwds.get("lon2", 0)

	def parse_line(self, parser):
		line = parser.current_line.strip()
		match = SectorRunway.REGEX.match(line)
		if not match:
			parser.raise_parse_error("invalid runway entry")
		self.name, self.opposite_name, self.heading, self.opposite_heading, \
				self.lat1, self.lon1, self.lat2, self.lon2 = match.groups()


class SectorNamedLineSegment:
	# name, lat1, lon1, lat2, lon2, [color]
	REGEX = re.compile(r"^(\S*)\s*(\S*)\s*(\S*)\s*(\S*)\s*(\S*)\s*(\S*)")

	def __init__(self, **kwds):
		self.name = kwds.get("name", "")
		self.lat1 = kwds.get("lat1", 0)
		self.lon1 = kwds.get("lon1", 0)
		self.lat2 = kwds.get("lat2", 0)
		self.lon2 = kwds.get("lon2", 0)
		self.color = kwds.get("color", 0)

	def parse_line(self, parser):
		line = parser.current_line.strip()
		match = SectorNamedLineSegment.REGEX.match(line)
		if not match:
			parser.raise_parse_error("invalid line segment")
		self.name, self.lat1, self.lon1, self.lat2, self.lon1, self.color = match.groups()


class SectorLineSegment:
	# lat1, lon1, lat2, lon2, [color]
	REGEX = re.compile(r"^(\S*)\s*(\S*)\s*(\S*)\s*(\S*)\s*(\S*)")

	def __init__(self, **kwds):
		self.lat1 = kwds.get("lat1", 0)
		self.lon1 = kwds.get("lon1", 0)
		self.lat2 = kwds.get("lat2", 0)
		self.lon2 = kwds.get("lon2", 0)
		self.color = kwds.get("color", 0)

	def parse_line(self, parser):
		line = parser.current_line.strip()
		match = SectorLineSegment.REGEX.match(line)
		if not match:
			parser.raise_parse_error("invalid line segment")
		self.lat1, self.lon1, self.lat2, self.lon1, self.color = match.groups()


class SectorSIDSTAR:
	# fixed 26 char name (empty on continuation), lat1, lon1, lat2, lon2, [color]
	REGEX = re.compile(r"^(.{26})\s*(\S*)\s*(\S*)\s*(\S*)\s*(\S*)(\S*)")

	def __init__(self, **kwds):
		self.name = kwds.get("name", "")
		self.segments = kwds.get("segments", [])

	def parse_line(self, parser):
		line = parser.current_line
		match = SectorSIDSTAR.REGEX.match(line)
		if not match:
			parser.raise_parse_error("invalid sid/star segment")
		name, lat1, lon1, lat2, lon2, color = match.groups()
		
		# name is fixed 26 chars, and empty except first line of a sid/star
		name = name.strip()

		if name: # start
			self.name = name
		
		seg = SectorNamedLineSegment()
		seg.name = self.name
		seg.lat1 = lat1
		seg.lon1 = lon1
		seg.lat2 = lat2
		seg.lon2 = lon2
		seg.color = color
		self.segments.append(seg)


class SectorParser:
	COORD_RE = re.compile(r"(N|S|E|W|n|s|e|w)(\d*).(\d*).(\d*.\d*)")

	def __init__(self, fileob):
		self.fileob = fileob
		self.line_count = 0
		self.line_number = 0
		self.current_line = ""
		self.line_buffer = []

		self.info = None #SectorInfo()
		self.colors = {}
		self.vor_map = {}
		self.ndb_map = {}
		self.fix_map = {}
		# TODO can airports be used as a coordinate..do I need lookup?
		self.airport_list = []
		self.runway_list = []
		self.artcc_map = {}
		self.artcc_low_map = {}
		self.artcc_high_map = {}
		self.airway_low_map = {}
		self.airway_high_map = {}
		self.sid_map = {}
		self.star_map = {}
		self.geo_list = []

	def parse(self):
		self.buffer_lines()

		while not self.eof():
			line = self.get_next_line().strip()

			if line[0] == "#":
				color = SectorColor.parse(self)
				self.colors[color.name] = color

			elif line[0] == "[":
				section = line[1:-1]
				if section == "INFO":
					self.info = SectorInfo.parse(self)
				elif section == "VOR":
					while not self.eof() and not self.next_line_starts_section():
						line = self.get_next_line()
						vor = SectorNavaid()
						vor.parse_line(self)
						self.vor_map[vor.name] = vor
				elif section == "NDB":
					while not self.eof() and not self.next_line_starts_section():
						line = self.get_next_line()
						ndb = SectorNavaid()
						ndb.parse_line(self)
						self.ndb_map[ndb.name] = ndb
				elif section == "FIX":
					while not self.eof() and not self.next_line_starts_section():
						line = self.get_next_line()
						fix = SectorNavaid()
						fix.parse_line(self)
						self.fix_map[fix.name] = fix
				elif section == "AIRPORT":
					while not self.eof() and not self.next_line_starts_section():
						line = self.get_next_line()
						apt = SectorAirport()
						apt.parse_line(self)
						self.airport_list.append(apt)
				elif section == "RUNWAY":
					while not self.eof() and not self.next_line_starts_section():
						line = self.get_next_line()
						runway = SectorRunway()
						runway.parse_line(self)
						self.runway_list.append(runway)
				elif section == "ARTCC":
					while not self.eof() and not self.next_line_starts_section():
						line = self.get_next_line()
						seg = SectorNamedLineSegment()
						seg.parse_line(self)
						if not seg.name in self.artcc_map:
							self.artcc_map[seg.name] = []
						self.artcc_map[seg.name].append(seg)
				elif section == "ARTCC HIGH":
					while not self.eof() and not self.next_line_starts_section():
						line = self.get_next_line()
						seg = SectorNamedLineSegment()
						seg.parse_line(self)
						if not seg.name in self.artcc_high_map:
							self.artcc_high_map[seg.name] = []
						self.artcc_high_map[seg.name].append(seg)
				elif section == "ARTCC LOW":
					while not self.eof() and not self.next_line_starts_section():
						line = self.get_next_line()
						seg = SectorNamedLineSegment()
						seg.parse_line(self)
						if not seg.name in self.artcc_low_map:
							self.artcc_low_map[seg.name] = []
						self.artcc_low_map[seg.name].append(seg)
				elif section == "AIRWAY LOW":
					while not self.eof() and not self.next_line_starts_section():
						line = self.get_next_line()
						seg = SectorNamedLineSegment()
						seg.parse_line(self)
						if not seg.name in self.airway_low_map:
							self.airway_low_map[seg.name] = []
						self.airway_low_map[seg.name].append(seg)
				elif section == "AIRWAY HIGH":
					while not self.eof() and not self.next_line_starts_section():
						line = self.get_next_line()
						seg = SectorNamedLineSegment()
						seg.parse_line(self)
						if not seg.name in self.airway_high_map:
							self.airway_high_map[seg.name] = []
						self.airway_high_map[seg.name].append(seg)
				elif section == "SID":
					# ughh...
					sidstar = None
					while not self.eof() and not self.next_line_starts_section():
						line = self.get_next_line()
						match = SectorSIDSTAR.REGEX.match(line)
						if not match:
							self.raise_parse_error("invalid sid/star segment")
						name, lat1, lon1, lat2, lon2, color = match.groups()
						if (name.strip()): # not empty => starting new sid/star
							sidstar = SectorSIDSTAR()
							sidstar.parse_line(self)
							self.sid_map[name] = sidstar
						else: # continues
							if not sidstar:
								self.raise_parse_error("invalid sid/star; no name")
							sidstar.parse_line(self)
				elif section == "STAR":
					# ughh...
					sidstar = None
					while not self.eof() and not self.next_line_starts_section():
						line = self.get_next_line()
						match = SectorSIDSTAR.REGEX.match(line)
						if not match:
							self.raise_parse_error("invalid sid/star segment")
						name, lat1, lon1, lat2, lon2, color = match.groups()
						if (name.strip()): # not empty => starting new sid/star
							sidstar = SectorSIDSTAR()
							sidstar.parse_line(self)
							self.star_map[name] = sidstar
						else: # continues
							if not sidstar:
								self.raise_parse_error("invalid sid/star; no name")
							sidstar.parse_line(self)
				elif section == "GEO":
					while not self.eof() and not self.next_line_starts_section():
						line = self.get_next_line()
						seg = SectorLineSegment()
						seg.parse_line(self)
						self.geo_list.append(seg)
				elif section == "REGIONS":
					pass #TODO
				elif section == "LABELS":
					pass #TODO

	def raise_parse_error(self, message):
		raise ParseError("parse error on line {0}: {1}".format(self.line_number, message))

	def eof(self):
		return len(self.line_buffer) == 0

	def next_line_starts_section(self):
		if not self.line_buffer:
			return False
		line, num = self.line_buffer[0]
		return line.strip()[0] == '['

	def get_next_line(self):
		if not self.line_buffer:
			return ""

		self.current_line, self.line_number = self.line_buffer.pop(0)

		if not self.line_buffer:
			self.buffer_lines()

		return self.current_line

	def buffer_lines(self):
		while len(self.line_buffer) < 30:
			line = self.fileob.readline()
			
			# eof
			if not line:
				return
			
			self.line_count += 1

			# empty or comment
			stripped = line.strip()
			if not stripped or stripped[0] == ';':
				continue

			# get rid of any trailing commments
			line = line.split(';')[0]

			# discard any trailing whitespace, but keep leading..it matters for sid/star
			line = line.rstrip()

			self.line_buffer.append((line, self.line_count))

	def parse_string_coords(self, lat_str, lon_str):
		match = SectorParser.COORD_RE.match(lat_str)
		if match:
			hemi, degs, mins, secs = match.groups()
			lat = float(degs) + float(mins) / 60.0 + float(secs) / 3600.0
			if hemi.lower() in ('s', 'w'):
				lat = -lat
		elif lat_str in self.ndb_map:
			lat = self.ndb_map[lat_str].lat
		elif lat_str in self.vor_map:
			lat = self.vor_map[lat_str].lat
		elif lat_str in self.fix_map:
			lat = self.fix_map[lat_str].lat
		else:
			self.raise_parse_error("invalid latitude string")
		
		match = SectorParser.COORD_RE.match(lat_str)
		if match:
			hemi, degs, mins, secs = match.groups()
			lon = float(degs) + float(mins) / 60.0 + float(secs) / 3600.0
			if hemi.lower() in ('s', 'w'):
				lon = -lon
		elif lon_str in self.ndb_map:
			lon = self.ndb_map[lon_str].lon
		elif lon_str in self.vor_map:
			lon = self.vor_map[lon_str].lon
		elif lon_str in self.fix_map:
			lon = self.fix_map[lon_str].lon
		else:
			self.raise_parse_error("invalid longitude string")

		return lat, lon


if __name__ == "__main__":
	parser = SectorParser(open("ZJX_1109.sct2"))
	parser.parse()
